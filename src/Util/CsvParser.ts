export class Csv{
    private lines: string[][] = [];

    public static parse(text: string){
        let lines: string[] = text.split(String.fromCharCode(10));

        let csv = new Csv();

        for (let i=0; i<lines.length; i++) {
            let line = lines[i];

            if (!line) {
                //alert("!line");
                continue;
            }

            let words: string[] = line.split(",");

            csv.lines.push(words);
        }

        return csv;
    }

    public getLines(){
        return this.lines;
    }

    public getHeader(){
        if(this.lines.length < 1){
            return [];
        }

        return this.lines[0];
    }

    public getDictionaryArray(){
        let dicArray: { [key: string]: string }[] = [];
        let header = this.getHeader();

        if(header.length < 1){
            return [];
        }

        for(let i=1; i<this.lines.length; ++i){
            let dic: { [column: string]: string } = {};
            for (let j = 0; j < header.length; ++j) {
                let column = header[j];
                let value = this.lines[i][j];
                dic[column] = value;
            }

            dicArray.push(dic);
        }

        return dicArray;
    }

    public static parseCsvFromFile(path: string) {
        let file: File = new File(path);

        if (!file.open("r", "", "")) {
            return new Csv();
        }

        let text: string = file.read();

        let csv: Csv = Csv.parse(text);
        file.close();

        return csv;
    }
}