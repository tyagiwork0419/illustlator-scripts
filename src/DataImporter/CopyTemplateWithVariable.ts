﻿///<reference types="types-for-adobe/Illustrator/2015.3"/>
import { SettingDialog, InputData, Control } from "../SettingDialog/SettingDialog";

//選択中のオブジェクトのリスト
var docRef = app.activeDocument;

//選択中のオリジナルのオブジェクト(GroupItem)
var selectedItems = docRef.selection;
var selectedItem = selectedItems[0];

(function(){
     //実行可能かどうかチェック
    if(!IsExecutable ()){
       return;
    }
    
    let inputDatas: InputData[] = [
    new InputData("copyNum","コピー数", Control.EditText, 1),
    new InputData("variableName", "変数名", Control.EditText, "変数"),
    new InputData("rowNum", "行の数", Control.EditText, 1),
    new InputData("offsetX", "Xのオフセット", Control.EditText, 0),
    new InputData("offsetY", "Yのオフセット", Control.EditText, 0),
    new InputData("deltaX", "Xの間隔", Control.EditText, 0),
    new InputData("deltaY", "Yの間隔", Control.EditText, 0),
]

    var dialog = SettingDialog.create("title", inputDatas, (settings) => {
            CopyWithVariable (settings.copyNum, settings.variableName, settings.rowNum, settings.offsetX, settings.offsetY, settings.deltaX, settings.deltaY)
        });
    
    dialog.show();
    
    
    function CopyWithVariable(copyNum: number, variableName: string, rowNum: number, offsetX: number, offsetY: number, deltaX: number, deltaY: number){

    
        
        for(let i=0; i<copyNum; i++){
            //グループごと複製
            var dpObj = selectedItem.duplicate();
    
            //複製したグループに変数追加
            var dpVariable = docRef.variables.add();
            dpVariable.kind = VariableKind.VISIBILITY;
            dpVariable.name = variableName + (i+1);
            dpObj.visibilityVariable = dpVariable;
        
            //行数
            var row = i%rowNum;
            //列数
            var column = Math.floor(i/rowNum);
            //配置
            dpObj.translate(offsetX + deltaX * column , offsetY + deltaY * row);
            
        }
            
        app.redraw();
    }

    function onCloseClicked(){
        return;
    }
    

    //実行可能かどうかチェック
    function IsExecutable(){
        if(!docRef){
             alert("ドキュメントを開いていません");
             return;
        }
        
        if(selectedItems.length != 1){
            alert("グループを一つだけ選択して下さい");
            return false;
        }
    
        var selectedItem = selectedItems[0];
        var selectedType = selectedItem.typename;

        if(selectedType != "GroupItem"){
            alert("it is not group item. the type is " + selectedType);
            return false;
        }
        
        return true;
    }

})();
