///<reference types="types-for-adobe/Illustrator/2015.3"/>
import { Csv } from "../Util/CsvParser";

enum ElementPlacement{
			INSIDE = 0,
			PLACEAFTER = 1,
			PLACEATBEGINNING = 2,
			PLACEATEND = 3,
			PLACEBEFORE = 4,
		}

class CSVConverter {
	textWords: string[] = [];
	//lines:string[][] = [];
	dictionaryArray: { [column: string]: string }[] = [];
	textGroups: GroupItem[] = [];
	pathGroups: GroupItem[] = [];
	variables = app.activeDocument.variables;
	path = String(app.documents[0].fullName).replace(app.documents[0].name, "");

	//CSVの何列目に画像ファイルの項目があるか
	imageIndex: number = 3; //3列目

	constructor() {
		for (var i = 0; i < this.variables.length; i++) {
			let pageItems = this.variables[i].pageItems;
			if(pageItems.length == 0){
				continue;
			}
			let group = pageItems[0] as GroupItem;
			if (group.textFrames.length != 0) {
				this.textGroups.push(group);
				continue;
			}

			if (group.pathItems.length != 0) {
				this.pathGroups.push(group);
				continue;
			}
		}
	}

	public read() {
		var file = File.openDialog("CSVファイルを選択してください。");
		if (!file) {
			return;
		}

		let csv: Csv = Csv.parseCsvFromFile(file.absoluteURI)
		this.textWords = csv.getHeader();
		//this.lines = csv.getLines();
		this.dictionaryArray = csv.getDictionaryArray();
	}

	public writeText() {
		for (var i = 0; i < this.textGroups.length; i++) {
			var textFrames = this.textGroups[i].textFrames;

			for (var j = 0; j < textFrames.length; j++) {
				var textFrame = textFrames[j];
				//var text = String(textFrame.contents);

				for (var key in this.textWords) {
					try {
						textFrame.contents = this.dictionaryArray[i][key];
						alert(this.dictionaryArray[i][key]);
					} catch (e) {
					}
				}
			}
		}
	}


	public writeImage(imageColumn: string) {
		for (var i = 0; i < this.pathGroups.length; i++) {
			var groups = this.pathGroups[i];
			var paths = [];

			if (groups.pathItems.length != 0) {
				paths = groups.pathItems;
			}

			for (var j = 0; j < paths.length; j++) {
				try {
					var rect = paths[j];
					var file = new File(this.path + this.dictionaryArray[i][imageColumn]);
					var pItem = app.activeDocument.placedItems.add();
					pItem.file = file;
					this.createPosition(pItem, rect);
					var mask = app.activeDocument.pathItems.rectangle(rect.top, rect.left, rect.width, rect.height);
					mask.stroked = true;
					mask.filled = true;
					var holder = app.activeDocument.groupItems.add();

					pItem.move(holder, ElementPlacement.PLACEATEND);
					mask.move(holder, ElementPlacement.PLACEATBEGINNING);
					holder.clipped = true;
					rect.remove();
				} catch (e) { }
			}
		}

	}

	private createPosition(targetA: PageItem, targetB: PageItem) {
		var widthA = targetA.width;
		var widthB = targetB.width;
		var heightA = targetA.height;
		var heightB = targetB.height;
		targetA.left = targetB.left;
		targetA.top = targetB.top;

		if (widthA > widthB) {
			targetA.left = targetA.left - ((widthA - widthB) / 2);
		}

		if (heightA > heightB) {
			targetA.top = targetA.top + ((heightA - heightB) / 2);
		}
	}



};

var converter = new CSVConverter();
converter.read();
alert("read");
converter.writeText();
alert("write text");
	//converter.writeImage();
