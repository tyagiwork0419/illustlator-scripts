﻿//<reference types="types-for-adobe/Illustrator/2015.3"/>

import "extendscript-es5-shim-ts";
import { Csv } from "../Util/CsvParser";

export class SettingDialog {
    window: Window;
    okButton: Button;
    cancelButton: Button;

    _editTextTable: {[key: string]: EditText} = {};
    _checkboxTable: {[key: string]: Checkbox} = {};
    _radioButtonTable: {[key: string]: RadioButton} = {};
    _dropDownListTable: {[key: string]: DropDownList} = {};


    windowSize = { w: 300, h: 300 };
    padding = { top: 20, bottom: 20, left: 20, right: 20 };
    space = { x: 5, y: 5 };

    staticTextSize = { w: 100, h: 20 };
    editTextSize = { w: 100, h: 20 };
    okButtonSize = { w: 100, h: 30 };
    cancelButtonSize = { w: 100, h: 30 };
    buttonTop = 200;
    buttonSpace = 110

    private constructor(title: string = "設定", onClickedOKCallback: (arg0: {[key: string]: any}) => void) {
        var that = this;
        this.window = new Window("dialog", title, [0, 0, this.windowSize.w, this.windowSize.h] as Bounds);

        this.okButton = this.window.add("button", [this.padding.left, this.buttonTop, this.padding.left + this.okButtonSize.w, this.buttonTop + this.okButtonSize.h] as Bounds, "実行");
        this.cancelButton = this.window.add("button", [this.padding.left + this.buttonSpace, this.buttonTop, this.padding.left + this.buttonSpace + this.cancelButtonSize.w, this.buttonTop + this.cancelButtonSize.h] as Bounds, "取消");

        this.okButton.onClick = function () {
            try{
                var inputs = that.getInputs();
                onClickedOKCallback(inputs);
                that.window.close();
            }catch(e){
                alert("c");
                throw e;
            }
        }

        this.cancelButton.onClick = function () {
            that.window.close();
        }
    };

    public static createFromCsv(title: string = "設定", path: string, onClickedOKCallback: (arg0: {[key: string]: any}) => void) {
        let dialog = new SettingDialog(title, onClickedOKCallback);

        let inputDatas: InputData[] = SettingDialog.parseCsvToInputData(path);
        dialog.setInputs(inputDatas);

        return dialog;
    }

    public static create(title: string = "設定", inputDatas: InputData[], onClickedOKCallback: (arg0: {[key: string]: any}) => void) {
        let dialog = new SettingDialog(title, onClickedOKCallback);

        dialog.setInputs(inputDatas);

        return dialog;
    }

    setInputs(inputDatas: InputData[]){
        this._editTextTable = {};
        this._checkboxTable = {};
        this._dropDownListTable = {};

        var staticTextSpace = this.staticTextSize.h + this.space.y;
        var editTextSpace = this.editTextSize.h + this.space.y;
        var editTextLeft = this.padding.left + this.staticTextSize.w + this.space.x;

        for(let i=0; i<inputDatas.length; ++i){
            let inputData = inputDatas[i];
            let staticText;
            let textBounds = [this.padding.left, this.padding.top + staticTextSpace * i, this.staticTextSize.w, this.staticTextSize.h] as Bounds;
            let controlBounds = [editTextLeft, this.padding.top + editTextSpace * i, editTextLeft + this.editTextSize.w, this.editTextSize.h + this.padding.top + editTextSpace * i] as Bounds;
            switch (inputData.control) {
                case Control.EditText:
                    staticText = this.window.add(Control.StaticText, textBounds, inputData.label);
                    let editText = this.window.add(Control.EditText, controlBounds, inputData.defaultValue);
                    this._editTextTable[inputData.key] = editText;
                    break;

                case Control.Checkbox:
                    let checkbox = this.window.add(Control.Checkbox, controlBounds, inputData.label);
                    checkbox.value = true;
                    this._checkboxTable[inputData.key] = checkbox;
                    break;

                case Control.RadioButton:
                    let radioButton = this.window.add(Control.RadioButton, controlBounds, inputData.label);
                    radioButton.value = true;
                    this._radioButtonTable[inputData.key] = radioButton;
                    break;

                case Control.DropDownList:
                    staticText = this.window.add(Control.StaticText, textBounds, inputData.label);
                    let dropDownList = this.window.add(Control.DropDownList, controlBounds, ["a","d","c","d"]);
                    dropDownList.selection = 0;
                    this._dropDownListTable[inputData.key] = dropDownList;
                    break;
            }
        }
    }

    private static parseCsvToInputData(path: string) {
        let csv: string[][] = Csv.parseCsvFromFile(path).getLines();
        let inputDatas: InputData[] = [];

        for (let row = 0; row < csv.length; ++row) {
            let value = csv[row];
            let key = value[0];
            let label = value[1];
            let control = value[2] as Control;
            let defaultValue = value[3];

            inputDatas.push(new InputData(key, label, control, defaultValue));
        }

        return inputDatas;
    }

    getInputs() {
        let data: { [key: string]: any } = {};
        Object.keys(this._editTextTable).forEach(key => {
            data[key] = this._editTextTable[key].text;
        });

        Object.keys(this._checkboxTable).forEach(key => {
            data[key] = this._checkboxTable[key].value;
        });

        Object.keys(this._radioButtonTable).forEach(key => {
            data[key] = this._radioButtonTable[key].value;
        });

        Object.keys(this._dropDownListTable).forEach(key => {
            data[key] = this._dropDownListTable[key].selection;
        });

        return data;
    }

    show() {
        this.window.center();
        this.window.show();
    }
}

export class InputData{
    key: string;
    control: Control;
    label: string;
    defaultValue: any;

    public constructor(key: string, label: string, control: Control, defaultValue: any){
        this.key = key;
        this.label = label;
        this.control = control;
        this.defaultValue = defaultValue;
    }
}

export enum Control{
    StaticText = "statictext",
    EditText = "edittext",
    Checkbox = "checkbox",
    RadioButton = "radiobutton",
    DropDownList = "dropdownlist",
}