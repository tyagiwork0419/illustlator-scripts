rem @echo off
set CURDIR=%CD%
set DESTDIR=%HOMEPATH%\git

if not exist %DESTDIR% mkdir %DESTDIR%
cd %DESTDIR%
git clone https://gitlab.com/tyagiwork0419/illustlator-scripts.git
cd illustlator-scripts
git pull origin main

cd dist

for %%a in ("*.js") do (
    echo %%a
)


cd %CURDIR%