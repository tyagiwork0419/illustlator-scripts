﻿/******/ (function () {
    /******/ var __webpack_modules__ = ({
        /***/ "./node_modules/extendscript-es5-shim-ts/index.js": 
        /*!********************************************************!*\
          !*** ./node_modules/extendscript-es5-shim-ts/index.js ***!
          \********************************************************/
        /***/ (function () {
            //every.js
            /*
            https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/every
            */
            if (!Array.prototype.every) {
                Array.prototype.every = function (callback, thisArg) {
                    var T, k;
                    if (this === void 0 || this === null) {
                        throw new TypeError('Array.prototype.every called on null or undefined');
                    }
                    // 1. Let O be the result of calling ToObject passing the this 
                    //    value as the argument.
                    var O = Object(this);
                    // 2. Let lenValue be the result of calling the Get internal method
                    //    of O with the argument "length".
                    // 3. Let len be ToUint32(lenValue).
                    var len = O.length >>> 0;
                    // 4. If IsCallable(callback) is false, throw a TypeError exception.
                    if (callback.__class__ !== 'Function') {
                        throw new TypeError(callback + ' is not a function');
                    }
                    // 5. If thisArg was supplied, let T be thisArg; else let T be undefined.
                    T = (arguments.length > 1) ? thisArg : void 0;
                    // 6. Let k be 0.
                    k = 0;
                    // 7. Repeat, while k < len
                    while (k < len) {
                        var kValue;
                        // a. Let Pk be ToString(k).
                        //   This is implicit for LHS operands of the in operator
                        // b. Let kPresent be the result of calling the HasProperty internal 
                        //    method of O with argument Pk.
                        //   This step can be combined with c
                        // c. If kPresent is true, then
                        if (k in O) {
                            // i. Let kValue be the result of calling the Get internal method
                            //    of O with argument Pk.
                            kValue = O[k];
                            // ii. Let testResult be the result of calling the Call internal method
                            //     of callback with T as the this value and argument list 
                            //     containing kValue, k, and O.
                            var testResult = callback.call(T, kValue, k, O);
                            // iii. If ToBoolean(testResult) is false, return false.
                            if (!testResult) {
                                return false;
                            }
                        }
                        k++;
                    }
                    return true;
                };
            }
            //filter.js
            /*
            https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter
            */
            if (!Array.prototype.filter) {
                Array.prototype.filter = function (callback, thisArg) {
                    if (this === void 0 || this === null) {
                        throw new TypeError('Array.prototype.filter called on null or undefined');
                    }
                    var t = Object(this);
                    var len = t.length >>> 0;
                    if (callback.__class__ !== 'Function') {
                        throw new TypeError(callback + ' is not a function');
                    }
                    var res = [];
                    var T = (arguments.length > 1) ? thisArg : void 0;
                    for (var i = 0; i < len; i++) {
                        if (i in t) {
                            var val = t[i];
                            // NOTE: Technically this should Object.defineProperty at
                            //       the next index, as push can be affected by
                            //       properties on Object.prototype and Array.prototype.
                            //       But that method's new, and collisions should be
                            //       rare, so use the more-compatible alternative.
                            if (callback.call(T, val, i, t)) {
                                res.push(val);
                            }
                        }
                    }
                    return res;
                };
            }
            //forEach.js
            /*
            https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach
            */
            // Production steps of ECMA-262, Edition 5, 15.4.4.18
            // Reference: http://es5.github.io/#x15.4.4.18
            if (!Array.prototype.forEach) {
                Array.prototype.forEach = function (callback, thisArg) {
                    if (this === void 0 || this === null) {
                        throw new TypeError('Array.prototype.forEach called on null or undefined');
                    }
                    // 1. Let O be the result of calling toObject() passing the
                    // |this| value as the argument.
                    var O = Object(this);
                    // 2. Let lenValue be the result of calling the Get() internal
                    // method of O with the argument "length".
                    // 3. Let len be toUint32(lenValue).
                    var len = O.length >>> 0;
                    // 4. If isCallable(callback) is false, throw a TypeError exception. 
                    // See: http://es5.github.com/#x9.11
                    if (callback.__class__ !== 'Function') {
                        throw new TypeError(callback + ' is not a function');
                    }
                    // 5. If thisArg was supplied, let T be thisArg; else let
                    // T be undefined.
                    var T = (arguments.length > 1) ? thisArg : void 0;
                    // 6. Let k be 0
                    //k = 0;
                    // 7. Repeat, while k < len
                    for (var k = 0; k < len; k++) {
                        var kValue;
                        // a. Let Pk be ToString(k).
                        //    This is implicit for LHS operands of the in operator
                        // b. Let kPresent be the result of calling the HasProperty
                        //    internal method of O with argument Pk.
                        //    This step can be combined with c
                        // c. If kPresent is true, then
                        if (k in O) {
                            // i. Let kValue be the result of calling the Get internal
                            // method of O with argument Pk.
                            kValue = O[k];
                            // ii. Call the Call internal method of callback with T as
                            // the this value and argument list containing kValue, k, and O.
                            callback.call(T, kValue, k, O);
                        }
                    }
                    // 8. return undefined
                };
            }
            //indexOf.js
            /*
            https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/indexOf#Polyfill
            */
            // Production steps of ECMA-262, Edition 5, 15.4.4.14
            // Reference: http://es5.github.io/#x15.4.4.14
            if (!Array.prototype.indexOf) {
                Array.prototype.indexOf = function (searchElement, fromIndex) {
                    // 1. Let o be the result of calling ToObject passing
                    //    the this value as the argument.
                    if (this === void 0 || this === null) {
                        throw new TypeError('Array.prototype.indexOf called on null or undefined');
                    }
                    var k;
                    var o = Object(this);
                    // 2. Let lenValue be the result of calling the Get
                    //    internal method of o with the argument "length".
                    // 3. Let len be ToUint32(lenValue).
                    var len = o.length >>> 0;
                    // 4. If len is 0, return -1.
                    if (len === 0) {
                        return -1;
                    }
                    // 5. If argument fromIndex was passed let n be
                    //    ToInteger(fromIndex); else let n be 0.
                    var n = +fromIndex || 0;
                    if (Math.abs(n) === Infinity) {
                        n = 0;
                    }
                    // 6. If n >= len, return -1.
                    if (n >= len) {
                        return -1;
                    }
                    // 7. If n >= 0, then Let k be n.
                    // 8. Else, n<0, Let k be len - abs(n).
                    //    If k is less than 0, then let k be 0.
                    k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);
                    // 9. Repeat, while k < len
                    while (k < len) {
                        // a. Let Pk be ToString(k).
                        //   This is implicit for LHS operands of the in operator
                        // b. Let kPresent be the result of calling the
                        //    HasProperty internal method of o with argument Pk.
                        //   This step can be combined with c
                        // c. If kPresent is true, then
                        //    i.  Let elementK be the result of calling the Get
                        //        internal method of o with the argument ToString(k).
                        //   ii.  Let same be the result of applying the
                        //        Strict Equality Comparison Algorithm to
                        //        searchElement and elementK.
                        //  iii.  If same is true, return k.
                        if (k in o && o[k] === searchElement) {
                            return k;
                        }
                        k++;
                    }
                    return -1;
                };
            }
            //isArray.js
            /*
            https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/isArray
            */
            if (!Array.isArray) {
                Array.isArray = function (arg) {
                    if (arg === void 0 || arg === null) {
                        return false;
                    }
                    return (arg.__class__ === 'Array');
                };
            }
            //lastIndexOf.js
            /*
            https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/lastIndexOf
            */
            // Production steps of ECMA-262, Edition 5, 15.4.4.15
            // Reference: http://es5.github.io/#x15.4.4.15
            if (!Array.prototype.lastIndexOf) {
                Array.prototype.lastIndexOf = function (searchElement, fromIndex) {
                    if (this === void 0 || this === null) {
                        throw new TypeError('Array.prototype.lastIndexOf called on null or undefined');
                    }
                    var n, k, t = Object(this), len = t.length >>> 0;
                    if (len === 0) {
                        return -1;
                    }
                    n = len - 1;
                    if (arguments.length > 1) {
                        n = Number(arguments[1]);
                        if (n != n) {
                            n = 0;
                        }
                        else if (n != 0 && n != Infinity && n != -Infinity) {
                            n = (n > 0 || -1) * Math.floor(Math.abs(n));
                        }
                    }
                    for (k = n >= 0 ? Math.min(n, len - 1) : len - Math.abs(n); k >= 0; k--) {
                        if (k in t && t[k] === searchElement) {
                            return k;
                        }
                    }
                    return -1;
                };
            }
            //map.js
            /*
            https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map
            */
            // Production steps of ECMA-262, Edition 5, 15.4.4.19
            // Reference: http://es5.github.io/#x15.4.4.19
            if (!Array.prototype.map) {
                Array.prototype.map = function (callback, thisArg) {
                    var T, A, k;
                    if (this === void 0 || this === null) {
                        throw new TypeError('Array.prototype.map called on null or undefined');
                    }
                    // 1. Let O be the result of calling ToObject passing the |this| 
                    //    value as the argument.
                    var O = Object(this);
                    // 2. Let lenValue be the result of calling the Get internal 
                    //    method of O with the argument "length".
                    // 3. Let len be ToUint32(lenValue).
                    var len = O.length >>> 0;
                    // 4. If IsCallable(callback) is false, throw a TypeError exception.
                    // See: http://es5.github.com/#x9.11
                    if (callback.__class__ !== 'Function') {
                        throw new TypeError(callback + ' is not a function');
                    }
                    // 5. If thisArg was supplied, let T be thisArg; else let T be undefined.
                    T = (arguments.length > 1) ? thisArg : void 0;
                    // 6. Let A be a new array created as if by the expression new Array(len) 
                    //    where Array is the standard built-in constructor with that name and 
                    //    len is the value of len.
                    A = new Array(len);
                    for (var k = 0; k < len; k++) {
                        var kValue, mappedValue;
                        // a. Let Pk be ToString(k).
                        //   This is implicit for LHS operands of the in operator
                        // b. Let kPresent be the result of calling the HasProperty internal 
                        //    method of O with argument Pk.
                        //   This step can be combined with c
                        // c. If kPresent is true, then
                        if (k in O) {
                            // i. Let kValue be the result of calling the Get internal 
                            //    method of O with argument Pk.
                            kValue = O[k];
                            // ii. Let mappedValue be the result of calling the Call internal 
                            //     method of callback with T as the this value and argument 
                            //     list containing kValue, k, and O.
                            mappedValue = callback.call(T, kValue, k, O);
                            // iii. Call the DefineOwnProperty internal method of A with arguments
                            // Pk, Property Descriptor
                            // { Value: mappedValue,
                            //   Writable: true,
                            //   Enumerable: true,
                            //   Configurable: true },
                            // and false.
                            // In browsers that support Object.defineProperty, use the following:
                            // Object.defineProperty(A, k, {
                            //   value: mappedValue,
                            //   writable: true,
                            //   enumerable: true,
                            //   configurable: true
                            // });
                            // For best browser support, use the following:
                            A[k] = mappedValue;
                        }
                    }
                    // 9. return A
                    return A;
                };
            }
            //reduce.js
            /*
            https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce
            */
            // Production steps of ECMA-262, Edition 5, 15.4.4.21
            // Reference: http://es5.github.io/#x15.4.4.21
            if (!Array.prototype.reduce) {
                Array.prototype.reduce = function (callback, initialValue) {
                    if (this === void 0 || this === null) {
                        throw new TypeError('Array.prototype.reduce called on null or undefined');
                    }
                    if (callback.__class__ !== 'Function') {
                        throw new TypeError(callback + ' is not a function');
                    }
                    var t = Object(this), len = t.length >>> 0, k = 0, value;
                    if (arguments.length > 1) {
                        value = initialValue;
                    }
                    else {
                        while (k < len && !(k in t)) {
                            k++;
                        }
                        if (k >= len) {
                            throw new TypeError('Reduce of empty array with no initial value');
                        }
                        value = t[k++];
                    }
                    for (; k < len; k++) {
                        if (k in t) {
                            value = callback(value, t[k], k, t);
                        }
                    }
                    return value;
                };
            }
            //reduceRight.js
            /*
            https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/ReduceRight
            */
            // Production steps of ECMA-262, Edition 5, 15.4.4.22
            // Reference: http://es5.github.io/#x15.4.4.22
            if (!Array.prototype.reduceRight) {
                Array.prototype.reduceRight = function (callback, initialValue) {
                    if (this === void 0 || this === null) {
                        throw new TypeError('Array.prototype.reduceRight called on null or undefined');
                    }
                    if (callback.__class__ !== 'Function') {
                        throw new TypeError(callback + ' is not a function');
                    }
                    var t = Object(this), len = t.length >>> 0, k = len - 1, value;
                    if (arguments.length > 1) {
                        value = initialValue;
                    }
                    else {
                        while (k >= 0 && !(k in t)) {
                            k--;
                        }
                        if (k < 0) {
                            throw new TypeError('Reduce of empty array with no initial value');
                        }
                        value = t[k--];
                    }
                    for (; k >= 0; k--) {
                        if (k in t) {
                            value = callback(value, t[k], k, t);
                        }
                    }
                    return value;
                };
            }
            //some.js
            /*
            https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/some
            */
            // Production steps of ECMA-262, Edition 5, 15.4.4.17
            // Reference: http://es5.github.io/#x15.4.4.17
            if (!Array.prototype.some) {
                Array.prototype.some = function (callback, thisArg) {
                    if (this === void 0 || this === null) {
                        throw new TypeError('Array.prototype.some called on null or undefined');
                    }
                    if (callback.__class__ !== 'Function') {
                        throw new TypeError(callback + ' is not a function');
                    }
                    var t = Object(this);
                    var len = t.length >>> 0;
                    var T = arguments.length > 1 ? thisArg : void 0;
                    for (var i = 0; i < len; i++) {
                        if (i in t && callback.call(T, t[i], i, t)) {
                            return true;
                        }
                    }
                    return false;
                };
            }
            //bind.js
            /*
            https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/bind#Polyfill
            
            WARNING! Bound functions used as constructors NOT supported by this polyfill!
            https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/bind#Bound_functions_used_as_constructors
            */
            if (!Function.prototype.bind) {
                Function.prototype.bind = function (oThis) {
                    if (this.__class__ !== 'Function') {
                        throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable');
                    }
                    var aArgs = Array.prototype.slice.call(arguments, 1), fToBind = this, fNOP = function () { }, fBound = function () {
                        return fToBind.apply(this instanceof fNOP
                            ? this
                            : oThis, aArgs.concat(Array.prototype.slice.call(arguments)));
                    };
                    if (this.prototype) {
                        // Function.prototype doesn't have a prototype property
                        fNOP.prototype = this.prototype;
                    }
                    fBound.prototype = new fNOP();
                    return fBound;
                };
            }
            //create.js
            if (!Object.create) {
                // Production steps of ECMA-262, Edition 5, 15.2.3.5
                // Reference: http://es5.github.io/#x15.2.3.5
                Object.create = (function () {
                    // To save on memory, use a shared constructor
                    function Temp() { }
                    // make a safe reference to Object.prototype.hasOwnProperty
                    var hasOwn = Object.prototype.hasOwnProperty;
                    return function (O) {
                        // 1. If Type(O) is not Object or Null throw a TypeError exception.
                        if (Object(O) !== O && O !== null) {
                            throw TypeError('Object prototype may only be an Object or null');
                        }
                        // 2. Let obj be the result of creating a new object as if by the
                        //    expression new Object() where Object is the standard built-in
                        //    constructor with that name
                        // 3. Set the [[Prototype]] internal property of obj to O.
                        Temp.prototype = O;
                        var obj = new Temp();
                        Temp.prototype = null; // Let's not keep a stray reference to O...
                        // 4. If the argument Properties is present and not undefined, add
                        //    own properties to obj as if by calling the standard built-in
                        //    function Object.defineProperties with arguments obj and
                        //    Properties.
                        if (arguments.length > 1) {
                            // Object.defineProperties does ToObject on its first argument.
                            var Properties = Object(arguments[1]);
                            for (var prop in Properties) {
                                if (hasOwn.call(Properties, prop)) {
                                    var descriptor = Properties[prop];
                                    if (Object(descriptor) !== descriptor) {
                                        throw TypeError(prop + 'must be an object');
                                    }
                                    if ('get' in descriptor || 'set' in descriptor) {
                                        throw new TypeError('getters & setters can not be defined on this javascript engine');
                                    }
                                    if ('value' in descriptor) {
                                        obj[prop] = Properties[prop];
                                    }
                                }
                            }
                        }
                        // 5. Return obj
                        return obj;
                    };
                })();
            }
            //defineProperties.js
            /*
            https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperties#Polyfill
            */
            if (!Object.defineProperties) {
                Object.defineProperties = function (object, props) {
                    function hasProperty(obj, prop) {
                        return Object.prototype.hasOwnProperty.call(obj, prop);
                    }
                    function convertToDescriptor(desc) {
                        if (Object(desc) !== desc) {
                            throw new TypeError('Descriptor can only be an Object.');
                        }
                        var d = {};
                        if (hasProperty(desc, "enumerable")) {
                            d.enumerable = !!desc.enumerable;
                        }
                        if (hasProperty(desc, "configurable")) {
                            d.configurable = !!desc.configurable;
                        }
                        if (hasProperty(desc, "value")) {
                            d.value = desc.value;
                        }
                        if (hasProperty(desc, "writable")) {
                            d.writable = !!desc.writable;
                        }
                        if (hasProperty(desc, "get")) {
                            throw new TypeError('getters & setters can not be defined on this javascript engine');
                        }
                        if (hasProperty(desc, "set")) {
                            throw new TypeError('getters & setters can not be defined on this javascript engine');
                        }
                        return d;
                    }
                    if (Object(object) !== object) {
                        throw new TypeError('Object.defineProperties can only be called on Objects.');
                    }
                    if (Object(props) !== props) {
                        throw new TypeError('Properties can only be an Object.');
                    }
                    var properties = Object(props);
                    for (propName in properties) {
                        if (hasOwnProperty.call(properties, propName)) {
                            var descr = convertToDescriptor(properties[propName]);
                            object[propName] = descr.value;
                        }
                    }
                    return object;
                };
            }
            //defineProperty.js
            if (!Object.defineProperty) {
                Object.defineProperty = function defineProperty(object, property, descriptor) {
                    if (Object(object) !== object) {
                        throw new TypeError('Object.defineProperty can only be called on Objects.');
                    }
                    if (Object(descriptor) !== descriptor) {
                        throw new TypeError('Property description can only be an Object.');
                    }
                    if ('get' in descriptor || 'set' in descriptor) {
                        throw new TypeError('getters & setters can not be defined on this javascript engine');
                    }
                    // If it's a data property.
                    if ('value' in descriptor) {
                        // fail silently if 'writable', 'enumerable', or 'configurable'
                        // are requested but not supported
                        // can't implement these features; allow true but not false
                        /* if (
                                 ('writable' in descriptor && !descriptor.writable) ||
                                 ('enumerable' in descriptor && !descriptor.enumerable) ||
                                 ('configurable' in descriptor && !descriptor.configurable)
                             )
                                 {
                                     throw new RangeError('This implementation of Object.defineProperty does not support configurable, enumerable, or writable properties SET to FALSE.');
                                 }*/
                        object[property] = descriptor.value;
                    }
                    return object;
                };
            }
            //freeze.js
            /*
            https://github.com/es-shims/es5-shim/blob/master/es5-sham.js
            */
            // ES5 15.2.3.9
            // http://es5.github.com/#x15.2.3.9
            if (!Object.freeze) {
                Object.freeze = function freeze(object) {
                    if (Object(object) !== object) {
                        throw new TypeError('Object.freeze can only be called on Objects.');
                    }
                    // this is misleading and breaks feature-detection, but
                    // allows "securable" code to "gracefully" degrade to working
                    // but insecure code.
                    return object;
                };
            }
            //getOwnPropertyDescriptor.js
            if (!Object.getOwnPropertyDescriptor) {
                Object.getOwnPropertyDescriptor = function getOwnPropertyDescriptor(object, property) {
                    if (Object(object) !== object) {
                        throw new TypeError('Object.getOwnPropertyDescriptor can only be called on Objects.');
                    }
                    var descriptor;
                    if (!Object.prototype.hasOwnProperty.call(object, property)) {
                        return descriptor;
                    }
                    descriptor = {
                        enumerable: Object.prototype.propertyIsEnumerable.call(object, property),
                        configurable: true
                    };
                    descriptor.value = object[property];
                    var psPropertyType = object.reflect.find(property).type;
                    descriptor.writable = !(psPropertyType === "readonly");
                    return descriptor;
                };
            }
            //getOwnPropertyNames.js
            if (!Object.getOwnPropertyNames) {
                Object.getOwnPropertyNames = function getOwnPropertyNames(object) {
                    if (Object(object) !== object) {
                        throw new TypeError('Object.getOwnPropertyNames can only be called on Objects.');
                    }
                    var names = [];
                    var hasOwnProperty = Object.prototype.hasOwnProperty;
                    var propertyIsEnumerable = Object.prototype.propertyIsEnumerable;
                    for (var prop in object) {
                        if (hasOwnProperty.call(object, prop)) {
                            names.push(prop);
                        }
                    }
                    var properties = object.reflect.properties;
                    var methods = object.reflect.methods;
                    var all = methods.concat(properties);
                    for (var i = 0; i < all.length; i++) {
                        var prop = all[i].name;
                        if (hasOwnProperty.call(object, prop) && !(propertyIsEnumerable.call(object, prop))) {
                            names.push(prop);
                        }
                    }
                    return names;
                };
            }
            //getPrototypeOf.js
            if (!Object.getPrototypeOf) {
                Object.getPrototypeOf = function (object) {
                    if (Object(object) !== object) {
                        throw new TypeError('Object.getPrototypeOf can only be called on Objects.');
                    }
                    return object.__proto__;
                };
            }
            //isExtensible.js
            // ES5 15.2.3.13
            // http://es5.github.com/#x15.2.3.13
            if (!Object.isExtensible) {
                Object.isExtensible = function isExtensible(object) {
                    if (Object(object) !== object) {
                        throw new TypeError('Object.isExtensible can only be called on Objects.');
                    }
                    return true;
                };
            }
            //isSealed.js
            /*
            https://github.com/es-shims/es5-shim/blob/master/es5-sham.js
            */
            // ES5 15.2.3.11
            // http://es5.github.com/#x15.2.3.11
            if (!Object.isSealed) {
                Object.isSealed = function isSealed(object) {
                    if (Object(object) !== object) {
                        throw new TypeError('Object.isSealed can only be called on Objects.');
                    }
                    return false;
                };
            }
            //isFrozen.js
            /*
            https://github.com/es-shims/es5-shim/blob/master/es5-sham.js
            */
            // ES5 15.2.3.12
            // http://es5.github.com/#x15.2.3.12
            if (!Object.isFrozen) {
                Object.isFrozen = function isFrozen(object) {
                    if (Object(object) !== object) {
                        throw new TypeError('Object.isFrozen can only be called on Objects.');
                    }
                    return false;
                };
            }
            //keys.js
            if (!Object.keys) {
                Object.keys = function (object) {
                    if (Object(object) !== object) {
                        throw new TypeError('Object.keys can only be called on Objects.');
                    }
                    var hasOwnProperty = Object.prototype.hasOwnProperty;
                    var result = [];
                    for (var prop in object) {
                        if (hasOwnProperty.call(object, prop)) {
                            result.push(prop);
                        }
                    }
                    return result;
                };
            }
            //preventExtensions.js
            /*
            https://github.com/es-shims/es5-shim/blob/master/es5-sham.js
            */
            // ES5 15.2.3.10
            // http://es5.github.com/#x15.2.3.10
            if (!Object.preventExtensions) {
                Object.preventExtensions = function preventExtensions(object) {
                    if (Object(object) !== object) {
                        throw new TypeError('Object.preventExtensions can only be called on Objects.');
                    }
                    // this is misleading and breaks feature-detection, but
                    // allows "securable" code to "gracefully" degrade to working
                    // but insecure code.
                    return object;
                };
            }
            //seal.js
            /*
            https://github.com/es-shims/es5-shim/blob/master/es5-sham.js
            */
            // ES5 15.2.3.8
            // http://es5.github.com/#x15.2.3.8
            if (!Object.seal) {
                Object.seal = function seal(object) {
                    if (Object(object) !== object) {
                        throw new TypeError('Object.seal can only be called on Objects.');
                    }
                    // this is misleading and breaks feature-detection, but
                    // allows "securable" code to "gracefully" degrade to working
                    // but insecure code.
                    return object;
                };
            }
            //parseAndStringify.js
            /*
                json2.js
                2014-02-04
            
                Public Domain.
            
                NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.
            
                See http://www.JSON.org/js.html
            
            
                This code should be minified before deployment.
                See http://javascript.crockford.com/jsmin.html
            
                USE YOUR OWN COPY. IT IS EXTREMELY UNWISE TO LOAD CODE FROM SERVERS YOU DO
                NOT CONTROL.
            
            
                This file creates a global JSON object containing two methods: stringify
                and parse.
            
                    JSON.stringify(value, replacer, space)
                        value       any JavaScript value, usually an object or array.
            
                        replacer    an optional parameter that determines how object
                                    values are stringified for objects. It can be a
                                    function or an array of strings.
            
                        space       an optional parameter that specifies the indentation
                                    of nested structures. If it is omitted, the text will
                                    be packed without extra whitespace. If it is a number,
                                    it will specify the number of spaces to indent at each
                                    level. If it is a string (such as '\t' or '&nbsp;'),
                                    it contains the characters used to indent at each level.
            
                        This method produces a JSON text from a JavaScript value.
            
                        When an object value is found, if the object contains a toJSON
                        method, its toJSON method will be called and the result will be
                        stringified. A toJSON method does not serialize: it returns the
                        value represented by the name/value pair that should be serialized,
                        or undefined if nothing should be serialized. The toJSON method
                        will be passed the key associated with the value, and this will be
                        bound to the value
            
                        For example, this would serialize Dates as ISO strings.
            
                            Date.prototype.toJSON = function (key) {
                                function f(n) {
                                    // Format integers to have at least two digits.
                                    return n < 10 ? '0' + n : n;
                                }
            
                                return this.getUTCFullYear()   + '-' +
                                     f(this.getUTCMonth() + 1) + '-' +
                                     f(this.getUTCDate())      + 'T' +
                                     f(this.getUTCHours())     + ':' +
                                     f(this.getUTCMinutes())   + ':' +
                                     f(this.getUTCSeconds())   + 'Z';
                            };
            
                        You can provide an optional replacer method. It will be passed the
                        key and value of each member, with this bound to the containing
                        object. The value that is returned from your method will be
                        serialized. If your method returns undefined, then the member will
                        be excluded from the serialization.
            
                        If the replacer parameter is an array of strings, then it will be
                        used to select the members to be serialized. It filters the results
                        such that only members with keys listed in the replacer array are
                        stringified.
            
                        Values that do not have JSON representations, such as undefined or
                        functions, will not be serialized. Such values in objects will be
                        dropped; in arrays they will be replaced with null. You can use
                        a replacer function to replace those with JSON values.
                        JSON.stringify(undefined) returns undefined.
            
                        The optional space parameter produces a stringification of the
                        value that is filled with line breaks and indentation to make it
                        easier to read.
            
                        If the space parameter is a non-empty string, then that string will
                        be used for indentation. If the space parameter is a number, then
                        the indentation will be that many spaces.
            
                        Example:
            
                        text = JSON.stringify(['e', {pluribus: 'unum'}]);
                        // text is '["e",{"pluribus":"unum"}]'
            
            
                        text = JSON.stringify(['e', {pluribus: 'unum'}], null, '\t');
                        // text is '[\n\t"e",\n\t{\n\t\t"pluribus": "unum"\n\t}\n]'
            
                        text = JSON.stringify([new Date()], function (key, value) {
                            return this[key] instanceof Date ?
                                'Date(' + this[key] + ')' : value;
                        });
                        // text is '["Date(---current time---)"]'
            
            
                    JSON.parse(text, reviver)
                        This method parses a JSON text to produce an object or array.
                        It can throw a SyntaxError exception.
            
                        The optional reviver parameter is a function that can filter and
                        transform the results. It receives each of the keys and values,
                        and its return value is used instead of the original value.
                        If it returns what it received, then the structure is not modified.
                        If it returns undefined then the member is deleted.
            
                        Example:
            
                        // Parse the text. Values that look like ISO date strings will
                        // be converted to Date objects.
            
                        myData = JSON.parse(text, function (key, value) {
                            var a;
                            if (typeof value === 'string') {
                                a =
            /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)Z$/.exec(value);
                                if (a) {
                                    return new Date(Date.UTC(+a[1], +a[2] - 1, +a[3], +a[4],
                                        +a[5], +a[6]));
                                }
                            }
                            return value;
                        });
            
                        myData = JSON.parse('["Date(09/09/2001)"]', function (key, value) {
                            var d;
                            if (typeof value === 'string' &&
                                    value.slice(0, 5) === 'Date(' &&
                                    value.slice(-1) === ')') {
                                d = new Date(value.slice(5, -1));
                                if (d) {
                                    return d;
                                }
                            }
                            return value;
                        });
            
            
                This is a reference implementation. You are free to copy, modify, or
                redistribute.
            */
            /*jslint evil: true, regexp: true */
            /*members "", "\b", "\t", "\n", "\f", "\r", "\"", JSON, "\\", apply,
                call, charCodeAt, getUTCDate, getUTCFullYear, getUTCHours,
                getUTCMinutes, getUTCMonth, getUTCSeconds, hasOwnProperty, join,
                lastIndex, length, parse, prototype, push, replace, slice, stringify,
                test, toJSON, toString, valueOf
            */
            // Create a JSON object only if one does not already exist. We create the
            // methods in a closure to avoid creating global variables.
            if (typeof JSON !== 'object') {
                JSON = {};
            }
            (function () {
                'use strict';
                function f(n) {
                    // Format integers to have at least two digits.
                    return n < 10 ? '0' + n : n;
                }
                if (typeof Date.prototype.toJSON !== 'function') {
                    Date.prototype.toJSON = function () {
                        return isFinite(this.valueOf())
                            ? this.getUTCFullYear() + '-' +
                                f(this.getUTCMonth() + 1) + '-' +
                                f(this.getUTCDate()) + 'T' +
                                f(this.getUTCHours()) + ':' +
                                f(this.getUTCMinutes()) + ':' +
                                f(this.getUTCSeconds()) + 'Z'
                            : null;
                    };
                    String.prototype.toJSON =
                        Number.prototype.toJSON =
                            Boolean.prototype.toJSON = function () {
                                return this.valueOf();
                            };
                }
                var cx, escapable, gap, indent, meta, rep;
                function quote(string) {
                    // If the string contains no control characters, no quote characters, and no
                    // backslash characters, then we can safely slap some quotes around it.
                    // Otherwise we must also replace the offending characters with safe escape
                    // sequences.
                    escapable.lastIndex = 0;
                    return escapable.test(string) ? '"' + string.replace(escapable, function (a) {
                        var c = meta[a];
                        return typeof c === 'string'
                            ? c
                            : '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
                    }) + '"' : '"' + string + '"';
                }
                function str(key, holder) {
                    // Produce a string from holder[key].
                    var i, // The loop counter.
                    k, // The member key.
                    v, // The member value.
                    length, mind = gap, partial, value = holder[key];
                    // If the value has a toJSON method, call it to obtain a replacement value.
                    if (value && typeof value === 'object' &&
                        typeof value.toJSON === 'function') {
                        value = value.toJSON(key);
                    }
                    // If we were called with a replacer function, then call the replacer to
                    // obtain a replacement value.
                    if (typeof rep === 'function') {
                        value = rep.call(holder, key, value);
                    }
                    // What happens next depends on the value's type.
                    switch (typeof value) {
                        case 'string':
                            return quote(value);
                        case 'number':
                            // JSON numbers must be finite. Encode non-finite numbers as null.
                            return isFinite(value) ? String(value) : 'null';
                        case 'boolean':
                        case 'null':
                            // If the value is a boolean or null, convert it to a string. Note:
                            // typeof null does not produce 'null'. The case is included here in
                            // the remote chance that this gets fixed someday.
                            return String(value);
                        // If the type is 'object', we might be dealing with an object or an array or
                        // null.
                        case 'object':
                            // Due to a specification blunder in ECMAScript, typeof null is 'object',
                            // so watch out for that case.
                            if (!value) {
                                return 'null';
                            }
                            // Make an array to hold the partial results of stringifying this object value.
                            gap += indent;
                            partial = [];
                            // Is the value an array?
                            if (Object.prototype.toString.apply(value) === '[object Array]') {
                                // The value is an array. Stringify every element. Use null as a placeholder
                                // for non-JSON values.
                                length = value.length;
                                for (i = 0; i < length; i += 1) {
                                    partial[i] = str(i, value) || 'null';
                                }
                                // Join all of the elements together, separated with commas, and wrap them in
                                // brackets.
                                v = partial.length === 0
                                    ? '[]'
                                    : gap
                                        ? '[\n' + gap + partial.join(',\n' + gap) + '\n' + mind + ']'
                                        : '[' + partial.join(',') + ']';
                                gap = mind;
                                return v;
                            }
                            // If the replacer is an array, use it to select the members to be stringified.
                            if (rep && typeof rep === 'object') {
                                length = rep.length;
                                for (i = 0; i < length; i += 1) {
                                    if (typeof rep[i] === 'string') {
                                        k = rep[i];
                                        v = str(k, value);
                                        if (v) {
                                            partial.push(quote(k) + (gap ? ': ' : ':') + v);
                                        }
                                    }
                                }
                            }
                            else {
                                // Otherwise, iterate through all of the keys in the object.
                                for (k in value) {
                                    if (Object.prototype.hasOwnProperty.call(value, k)) {
                                        v = str(k, value);
                                        if (v) {
                                            partial.push(quote(k) + (gap ? ': ' : ':') + v);
                                        }
                                    }
                                }
                            }
                            // Join all of the member texts together, separated with commas,
                            // and wrap them in braces.
                            v = partial.length === 0
                                ? '{}'
                                : gap
                                    ? '{\n' + gap + partial.join(',\n' + gap) + '\n' + mind + '}'
                                    : '{' + partial.join(',') + '}';
                            gap = mind;
                            return v;
                    }
                }
                // If the JSON object does not yet have a stringify method, give it one.
                if (typeof JSON.stringify !== 'function') {
                    escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;
                    meta = {
                        '\b': '\\b',
                        '\t': '\\t',
                        '\n': '\\n',
                        '\f': '\\f',
                        '\r': '\\r',
                        '"': '\\"',
                        '\\': '\\\\'
                    };
                    JSON.stringify = function (value, replacer, space) {
                        // The stringify method takes a value and an optional replacer, and an optional
                        // space parameter, and returns a JSON text. The replacer can be a function
                        // that can replace values, or an array of strings that will select the keys.
                        // A default replacer method can be provided. Use of the space parameter can
                        // produce text that is more easily readable.
                        var i;
                        gap = '';
                        indent = '';
                        // If the space parameter is a number, make an indent string containing that
                        // many spaces.
                        if (typeof space === 'number') {
                            for (i = 0; i < space; i += 1) {
                                indent += ' ';
                            }
                            // If the space parameter is a string, it will be used as the indent string.
                        }
                        else if (typeof space === 'string') {
                            indent = space;
                        }
                        // If there is a replacer, it must be a function or an array.
                        // Otherwise, throw an error.
                        rep = replacer;
                        if (replacer && typeof replacer !== 'function' &&
                            (typeof replacer !== 'object' ||
                                typeof replacer.length !== 'number')) {
                            throw new Error('JSON.stringify');
                        }
                        // Make a fake root object containing our value under the key of ''.
                        // Return the result of stringifying the value.
                        return str('', { '': value });
                    };
                }
                // If the JSON object does not yet have a parse method, give it one.
                if (typeof JSON.parse !== 'function') {
                    cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;
                    JSON.parse = function (text, reviver) {
                        // The parse method takes a text and an optional reviver function, and returns
                        // a JavaScript value if the text is a valid JSON text.
                        var j;
                        function walk(holder, key) {
                            // The walk method is used to recursively walk the resulting structure so
                            // that modifications can be made.
                            var k, v, value = holder[key];
                            if (value && typeof value === 'object') {
                                for (k in value) {
                                    if (Object.prototype.hasOwnProperty.call(value, k)) {
                                        v = walk(value, k);
                                        if (v !== undefined) {
                                            value[k] = v;
                                        }
                                        else {
                                            delete value[k];
                                        }
                                    }
                                }
                            }
                            return reviver.call(holder, key, value);
                        }
                        // Parsing happens in four stages. In the first stage, we replace certain
                        // Unicode characters with escape sequences. JavaScript handles many characters
                        // incorrectly, either silently deleting them, or treating them as line endings.
                        text = String(text);
                        cx.lastIndex = 0;
                        if (cx.test(text)) {
                            text = text.replace(cx, function (a) {
                                return '\\u' +
                                    ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
                            });
                        }
                        // In the second stage, we run the text against regular expressions that look
                        // for non-JSON patterns. We are especially concerned with '()' and 'new'
                        // because they can cause invocation, and '=' because it can cause mutation.
                        // But just to be safe, we want to reject all unexpected forms.
                        // We split the second stage into 4 regexp operations in order to work around
                        // crippling inefficiencies in IE's and Safari's regexp engines. First we
                        // replace the JSON backslash pairs with '@' (a non-JSON character). Second, we
                        // replace all simple value tokens with ']' characters. Third, we delete all
                        // open brackets that follow a colon or comma or that begin the text. Finally,
                        // we look to see that the remaining characters are only whitespace or ']' or
                        // ',' or ':' or '{' or '}'. If that is so, then the text is safe for eval.
                        if (/^[\],:{}\s]*$/
                            .test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@')
                            .replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']')
                            .replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
                            // In the third stage we use the eval function to compile the text into a
                            // JavaScript structure. The '{' operator is subject to a syntactic ambiguity
                            // in JavaScript: it can begin a block or an object literal. We wrap the text
                            // in parens to eliminate the ambiguity.
                            j = eval('(' + text + ')');
                            // In the optional fourth stage, we recursively walk the new structure, passing
                            // each name/value pair to a reviver function for possible transformation.
                            return typeof reviver === 'function'
                                ? walk({ '': j }, '')
                                : j;
                        }
                        // If the text is not JSON parseable, then a SyntaxError is thrown.
                        throw new SyntaxError('JSON.parse');
                    };
                }
            }());
            //trim.js
            /*
            https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
            */
            if (!String.prototype.trim) {
                // Вырезаем BOM и неразрывный пробел
                String.prototype.trim = function () {
                    return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
                };
            }
            /***/ 
        }),
        /***/ "./src/SettingDialog/SettingDialog.ts": 
        /*!********************************************!*\
          !*** ./src/SettingDialog/SettingDialog.ts ***!
          \********************************************/
        /***/ (function (__unused_webpack_module, exports, __webpack_require__) {
            "use strict";
            //<reference types="types-for-adobe/Illustrator/2015.3"/>
            exports.__esModule = true;
            exports.Control = exports.InputData = exports.SettingDialog = void 0;
            __webpack_require__(/*! extendscript-es5-shim-ts */ "./node_modules/extendscript-es5-shim-ts/index.js");
            var CsvParser_1 = __webpack_require__(/*! ../Util/CsvParser */ "./src/Util/CsvParser.ts");
            var SettingDialog = /** @class */ (function () {
                function SettingDialog(title, onClickedOKCallback) {
                    if (title === void 0) {
                        title = "設定";
                    }
                    this._editTextTable = {};
                    this._checkboxTable = {};
                    this._radioButtonTable = {};
                    this._dropDownListTable = {};
                    this.windowSize = { w: 300, h: 300 };
                    this.padding = { top: 20, bottom: 20, left: 20, right: 20 };
                    this.space = { x: 5, y: 5 };
                    this.staticTextSize = { w: 100, h: 20 };
                    this.editTextSize = { w: 100, h: 20 };
                    this.okButtonSize = { w: 100, h: 30 };
                    this.cancelButtonSize = { w: 100, h: 30 };
                    this.buttonTop = 200;
                    this.buttonSpace = 110;
                    var that = this;
                    this.window = new Window("dialog", title, [0, 0, this.windowSize.w, this.windowSize.h]);
                    this.okButton = this.window.add("button", [this.padding.left, this.buttonTop, this.padding.left + this.okButtonSize.w, this.buttonTop + this.okButtonSize.h], "実行");
                    this.cancelButton = this.window.add("button", [this.padding.left + this.buttonSpace, this.buttonTop, this.padding.left + this.buttonSpace + this.cancelButtonSize.w, this.buttonTop + this.cancelButtonSize.h], "取消");
                    this.okButton.onClick = function () {
                        try {
                            var inputs = that.getInputs();
                            onClickedOKCallback(inputs);
                            that.window.close();
                        }
                        catch (e) {
                            alert("c");
                            throw e;
                        }
                    };
                    this.cancelButton.onClick = function () {
                        that.window.close();
                    };
                }
                ;
                SettingDialog.createFromCsv = function (title, path, onClickedOKCallback) {
                    if (title === void 0) {
                        title = "設定";
                    }
                    var dialog = new SettingDialog(title, onClickedOKCallback);
                    var inputDatas = SettingDialog.parseCsvToInputData(path);
                    dialog.setInputs(inputDatas);
                    return dialog;
                };
                SettingDialog.create = function (title, inputDatas, onClickedOKCallback) {
                    if (title === void 0) {
                        title = "設定";
                    }
                    var dialog = new SettingDialog(title, onClickedOKCallback);
                    dialog.setInputs(inputDatas);
                    return dialog;
                };
                SettingDialog.prototype.setInputs = function (inputDatas) {
                    this._editTextTable = {};
                    this._checkboxTable = {};
                    this._dropDownListTable = {};
                    var staticTextSpace = this.staticTextSize.h + this.space.y;
                    var editTextSpace = this.editTextSize.h + this.space.y;
                    var editTextLeft = this.padding.left + this.staticTextSize.w + this.space.x;
                    for (var i = 0; i < inputDatas.length; ++i) {
                        var inputData = inputDatas[i];
                        var staticText = void 0;
                        var textBounds = [this.padding.left, this.padding.top + staticTextSpace * i, this.staticTextSize.w, this.staticTextSize.h];
                        var controlBounds = [editTextLeft, this.padding.top + editTextSpace * i, editTextLeft + this.editTextSize.w, this.editTextSize.h + this.padding.top + editTextSpace * i];
                        switch (inputData.control) {
                            case Control.EditText:
                                staticText = this.window.add(Control.StaticText, textBounds, inputData.label);
                                var editText = this.window.add(Control.EditText, controlBounds, inputData.defaultValue);
                                this._editTextTable[inputData.key] = editText;
                                break;
                            case Control.Checkbox:
                                var checkbox = this.window.add(Control.Checkbox, controlBounds, inputData.label);
                                checkbox.value = true;
                                this._checkboxTable[inputData.key] = checkbox;
                                break;
                            case Control.RadioButton:
                                var radioButton = this.window.add(Control.RadioButton, controlBounds, inputData.label);
                                radioButton.value = true;
                                this._radioButtonTable[inputData.key] = radioButton;
                                break;
                            case Control.DropDownList:
                                staticText = this.window.add(Control.StaticText, textBounds, inputData.label);
                                var dropDownList = this.window.add(Control.DropDownList, controlBounds, ["a", "d", "c", "d"]);
                                dropDownList.selection = 0;
                                this._dropDownListTable[inputData.key] = dropDownList;
                                break;
                        }
                    }
                };
                SettingDialog.parseCsvToInputData = function (path) {
                    var csv = CsvParser_1.Csv.parseCsvFromFile(path).getLines();
                    var inputDatas = [];
                    for (var row = 0; row < csv.length; ++row) {
                        var value = csv[row];
                        var key = value[0];
                        var label = value[1];
                        var control = value[2];
                        var defaultValue = value[3];
                        inputDatas.push(new InputData(key, label, control, defaultValue));
                    }
                    return inputDatas;
                };
                SettingDialog.prototype.getInputs = function () {
                    var _this = this;
                    var data = {};
                    Object.keys(this._editTextTable).forEach(function (key) {
                        data[key] = _this._editTextTable[key].text;
                    });
                    Object.keys(this._checkboxTable).forEach(function (key) {
                        data[key] = _this._checkboxTable[key].value;
                    });
                    Object.keys(this._radioButtonTable).forEach(function (key) {
                        data[key] = _this._radioButtonTable[key].value;
                    });
                    Object.keys(this._dropDownListTable).forEach(function (key) {
                        data[key] = _this._dropDownListTable[key].selection;
                    });
                    return data;
                };
                SettingDialog.prototype.show = function () {
                    this.window.center();
                    this.window.show();
                };
                return SettingDialog;
            }());
            exports.SettingDialog = SettingDialog;
            var InputData = /** @class */ (function () {
                function InputData(key, label, control, defaultValue) {
                    this.key = key;
                    this.label = label;
                    this.control = control;
                    this.defaultValue = defaultValue;
                }
                return InputData;
            }());
            exports.InputData = InputData;
            var Control;
            (function (Control) {
                Control["StaticText"] = "statictext";
                Control["EditText"] = "edittext";
                Control["Checkbox"] = "checkbox";
                Control["RadioButton"] = "radiobutton";
                Control["DropDownList"] = "dropdownlist";
            })(Control = exports.Control || (exports.Control = {}));
            /***/ 
        }),
        /***/ "./src/Util/CsvParser.ts": 
        /*!*******************************!*\
          !*** ./src/Util/CsvParser.ts ***!
          \*******************************/
        /***/ (function (__unused_webpack_module, exports) {
            "use strict";
            exports.__esModule = true;
            exports.Csv = void 0;
            var Csv = /** @class */ (function () {
                function Csv() {
                    this.lines = [];
                }
                Csv.parse = function (text) {
                    var lines = text.split(String.fromCharCode(10));
                    var csv = new Csv();
                    for (var i = 0; i < lines.length; i++) {
                        var line = lines[i];
                        if (!line) {
                            //alert("!line");
                            continue;
                        }
                        var words = line.split(",");
                        csv.lines.push(words);
                    }
                    return csv;
                };
                Csv.prototype.getLines = function () {
                    return this.lines;
                };
                Csv.prototype.getHeader = function () {
                    if (this.lines.length < 1) {
                        return [];
                    }
                    return this.lines[0];
                };
                Csv.prototype.getDictionaryArray = function () {
                    var dicArray = [];
                    var header = this.getHeader();
                    if (header.length < 1) {
                        return [];
                    }
                    for (var i = 1; i < this.lines.length; ++i) {
                        var dic = {};
                        for (var j = 0; j < header.length; ++j) {
                            var column = header[j];
                            var value = this.lines[i][j];
                            dic[column] = value;
                        }
                        dicArray.push(dic);
                    }
                    return dicArray;
                };
                Csv.parseCsvFromFile = function (path) {
                    var file = new File(path);
                    if (!file.open("r", "", "")) {
                        return new Csv();
                    }
                    var text = file.read();
                    var csv = Csv.parse(text);
                    file.close();
                    return csv;
                };
                return Csv;
            }());
            exports.Csv = Csv;
            /***/ 
        })
        /******/ 
    });
    /************************************************************************/
    /******/ // The module cache
    /******/ var __webpack_module_cache__ = {};
    /******/
    /******/ // The require function
    /******/ function __webpack_require__(moduleId) {
        /******/ // Check if module is in cache
        /******/ var cachedModule = __webpack_module_cache__[moduleId];
        /******/ if (cachedModule !== undefined) {
            /******/ return cachedModule.exports;
            /******/ }
        /******/ // Create a new module (and put it into the cache)
        /******/ var module = __webpack_module_cache__[moduleId] = {
            /******/ // no module.id needed
            /******/ // no module.loaded needed
            /******/ exports: {}
            /******/ 
        };
        /******/
        /******/ // Execute the module function
        /******/ __webpack_modules__[moduleId](module, module.exports, __webpack_require__);
        /******/
        /******/ // Return the exports of the module
        /******/ return module.exports;
        /******/ 
    }
    /******/
    /************************************************************************/
    var __webpack_exports__ = {};
    // This entry need to be wrapped in an IIFE because it need to be in strict mode.
    (function () {
        "use strict";
        var exports = __webpack_exports__;
        /*!**********************************************!*\
          !*** ./src/CustomTrimMark/CustomTrimMark.ts ***!
          \**********************************************/
        ///<reference types="types-for-adobe/Illustrator/2015.3"
        exports.__esModule = true;
        var SettingDialog_1 = __webpack_require__(/*! ../SettingDialog/SettingDialog */ "./src/SettingDialog/SettingDialog.ts");
        __webpack_require__(/*! extendscript-es5-shim-ts */ "./node_modules/extendscript-es5-shim-ts/index.js");
        var CustomTrimMark = /** @class */ (function () {
            function CustomTrimMark(document, color, locked, stroke, drawVerticalCenter, drawBesideCenter) {
                if (locked === void 0) {
                    locked = true;
                }
                if (stroke === void 0) {
                    stroke = 0.3;
                }
                if (drawVerticalCenter === void 0) {
                    drawVerticalCenter = false;
                }
                if (drawBesideCenter === void 0) {
                    drawBesideCenter = false;
                }
                this.doc = document;
                this.color = color;
                this.locked = locked;
                this.stroke = stroke;
                this.drawVerticalCenter = drawVerticalCenter;
                this.drawBesideCenter = drawBesideCenter;
            }
            ;
            CustomTrimMark.prototype.draw = function () {
                var group = this.doc.groupItems.add();
                group.locked = this.locked;
                var pathItems = group.pathItems;
                var selObj = this.doc.selection;
                if (selObj.length < 1) {
                    alert("nothing is selected");
                    throw new Error("何も選択されていません");
                }
                var rect = app.activeDocument.selection[0].geometricBounds;
                var x1 = rect[0];
                var y1 = rect[1];
                var x2 = rect[2];
                var y2 = rect[3];
                var d = 20; //線の長さ
                var value = 8;
                var o = value + this.stroke;
                //日本式トンボ
                this._drawCorner(pathItems, o, d, x1, y1, x2, y2);
                if (this.drawVerticalCenter) {
                    this._drawVerticalCenter(pathItems, o, d, x1, y1, x2, y2);
                }
                if (this.drawBesideCenter) {
                    this._drawBesideCenter(pathItems, o, d, x1, y1, x2, y2);
                }
            };
            CustomTrimMark.prototype._drawCorner = function (pathItems, o, d, x1, y1, x2, y2) {
                this._drawLine3(pathItems, x1 - (d * 1.7), y1 + o, x1, y1 + o, x1, y1 + o + (d * 1.3)); //左上縦
                this._drawLine3(pathItems, x1 - (d * 1.7), y1 - o + o, x1 - o, y1 - o + o, x1 - o, y1 + o + (d * 1.3)); //左上縦
                this._drawLine3(pathItems, x2, y1 + o + (d * 1.3), x2, y1 + o, x2 + (d * 1.7), y1 + o); //右上縦
                this._drawLine3(pathItems, x2 + o, y1 + o + (d * 1.3), x2 + o, y1 + o - o, x2 + (d * 1.7), y1 + o - o); //左上横
                this._drawLine3(pathItems, x1 - (d * 1.7), y2 - o, x1, y2 - o, x1, y2 - o - (d * 1.3)); //左下縦
                this._drawLine3(pathItems, x1 - (d * 1.7), y2, x1 - o, y2, x1 - o, y2 - o - (d * 1.3)); //左下横
                this._drawLine3(pathItems, x2, y2 - o - (d * 1.3), x2, y2 - o, x2 + (d * 1.7), y2 - o); //右下縦
                this._drawLine3(pathItems, x2 + o, y2 - o - (d * 1.3), x2 + o, y2 + o - o, x2 + (d * 1.7), y2 + o - o); //左下横
            };
            CustomTrimMark.prototype._drawCenter = function (pathItems, o, d, x1, y1, x2, y2) {
                this._drawBesideCenter(pathItems, o, d, x1, x2, y1, y2);
                this._drawVerticalCenter(pathItems, o, d, x1, x2, y1, y2);
            };
            CustomTrimMark.prototype._drawVerticalCenter = function (pathItems, o, d, x1, y1, x2, y2) {
                this._drawLine2(pathItems, (x1 + x2) / 2 - (o * 4.4), y1 + (o * 2.2), (x1 + x2) / 2 + (o * 4.4), y1 + (o * 2.2)); //中央上横
                this._drawLine2(pathItems, (x1 + x2) / 2, y1 + (o * 1.5), (x1 + x2) / 2, y1 + o + (d * 1.4)); //中央上縦
                this._drawLine2(pathItems, (x1 + x2) / 2 - (o * 4.4), y2 - (o * 2.2), (x1 + x2) / 2 + (o * 4.4), y2 - (o * 2.2)); //中央下横
                this._drawLine2(pathItems, (x1 + x2) / 2, y2 - (o * 1.5), (x1 + x2) / 2, y2 - o - (d * 1.4)); //中央下縦
            };
            CustomTrimMark.prototype._drawBesideCenter = function (pathItems, o, d, x1, y1, x2, y2) {
                this._drawLine2(pathItems, x1 - (d * 1.8), (y1 + y2) / 2, x1 - (d * 0.6), (y1 + y2) / 2); //中央左横
                this._drawLine2(pathItems, x1 - (d * 0.9), (y1 + y2) / 2 + (o * 4.4), x1 - (d * 0.9), (y1 + y2) / 2 - (o * 4.4)); //中央左横
                this._drawLine2(pathItems, x2 + (d * 1.8), (y1 + y2) / 2, x2 + (d * 0.6), (y1 + y2) / 2); //中央右横
                this._drawLine2(pathItems, x2 + (d * 0.9), (y1 + y2) / 2 + (o * 4.4), x2 + (d * 0.9), (y1 + y2) / 2 - (o * 4.4)); //中央右横
            };
            // 線を描画(始点・終点の座標・2点)
            CustomTrimMark.prototype._drawLine2 = function (pathItems, sx, sy, ex, ey) {
                var pObj = pathItems.add();
                pObj.setEntirePath([
                    [sx, sy],
                    [ex, ey]
                ]);
                pObj.filled = false; //　塗りなし
                pObj.stroked = true; //　線あり
                pObj.strokeWidth = this.stroke; //　線幅1ポイント
                pObj.strokeColor = this.color; //　線の色を指定
                //pObj.guides = true;
            };
            // 線を描画(始点・終点の座標・3点)
            CustomTrimMark.prototype._drawLine3 = function (pathItems, x1, y1, x2, y2, x3, y3) {
                var pObj = pathItems.add();
                pObj.setEntirePath([
                    [x1, y1],
                    [x2, y2],
                    [x3, y3]
                ]);
                pObj.filled = false; //　塗りなし
                pObj.stroked = true; //　線あり
                pObj.strokeWidth = this.stroke; //　線幅1ポイント
                pObj.strokeColor = this.color; //　線の色を指定
                //pObj.guides = true;
            };
            // カラーを設定
            CustomTrimMark.prototype._setColor = function (r, g, b) {
                var tmpColor = new RGBColor();
                tmpColor.red = r;
                tmpColor.green = g;
                tmpColor.blue = b;
                return tmpColor;
            };
            return CustomTrimMark;
        }());
        var settings = [
            new SettingDialog_1.InputData("color", "色", SettingDialog_1.Control.DropDownList, "1"),
            new SettingDialog_1.InputData("stroke", "線の太さ", SettingDialog_1.Control.EditText, "0.3"),
            new SettingDialog_1.InputData("locked", "ロック", SettingDialog_1.Control.Checkbox, true),
            new SettingDialog_1.InputData("drawVerticalCenter", "縦の中心トンボ", SettingDialog_1.Control.Checkbox, true),
            new SettingDialog_1.InputData("drawBesideCenter", "横の中心トンボ", SettingDialog_1.Control.Checkbox, false),
        ];
        function main() {
            var onClickOk = function (data) {
                var doc = app.activeDocument;
                //let message: string = "";
                //Object.keys(data).forEach(key => {
                //    message += data[key] + ", \n";
                //});
                //alert(message);
                var color = new RGBColor();
                color.red = 0;
                color.green = 0;
                color.blue = 0;
                var stroke = parseFloat(data["stroke"]);
                var locked = data["locked"];
                var drawVerticalCenter = data["drawVerticalCenter"];
                var drawBesideCenter = data["drawBesideCenter"];
                try {
                    var trimMark = new CustomTrimMark(doc, color, locked, stroke, drawVerticalCenter, drawBesideCenter);
                    trimMark.draw();
                }
                catch (e) {
                    alert("error");
                    if (e instanceof Error) {
                        alert(e.toString());
                    }
                }
            };
            var dialog = SettingDialog_1.SettingDialog.create("title", settings, onClickOk);
            dialog.show();
        }
        main();
    })();
    /******/ 
})();
//# sourceMappingURL=CustomTrimMark.js.map
