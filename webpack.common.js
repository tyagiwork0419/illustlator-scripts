const path = require('path');

module.exports = {
    //let filename = env.name;
    //let entry = './src/' + filename + '.ts';
    //let output = __dirname + '/dist/' + filename + '.js';

    //console.log('filename = ' + filename);
    //console.log(entry);
    //console.log(output);

    //    entry: './src/' + filename + '.ts',
    output: {
        //path: path.resolve(__dirname, dist),
        path: __dirname + '/dist',
    },
    module: {
        rules: [
            {
                test: /.ts/,
                use: 'ts-loader',
                //options: {
                //    presets: [['@babel/preset-env', {loose: true, modules: 'commonjs'}], '@babel/preset-typescript'],
                //}
                exclude: /(node_modules)/
            }
        ]
    },
    resolve: {
        extensions: ['.ts', '.js']
    },
    optimization: {
        minimize: false
    },


}
